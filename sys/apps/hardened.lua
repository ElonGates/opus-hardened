local UI = require("opus.ui")
local Util = require("opus.util")

local function loadDirectory(dir)
	local plugins = { }
	for _, file in pairs(fs.list(dir)) do
		if file ~= "hardened" and not fs.isDir(file) then 
			local s, m = Util.run(_ENV, fs.combine(dir, file))
			if not s and m then
				_G.printError('Error loading: ' .. file)
				error(m or 'Unknown error')
			elseif s and m then
				table.insert(plugins, { tab = m, name = m.title, description = m.description })
			end
		end
	end
	return plugins
end

local programDir = fs.getDir(_ENV.arg[0])
local configs = loadDirectory(fs.combine(programDir, 'system/hardened'), { })

local page = UI.Page {
    notification = UI.Notification { },
    tabs = UI.Tabs {
        settings = UI.Tab {
            title = "Category",
            grid = UI.ScrollingGrid {
                x=2, y=2, ex=-2, ey=-2,
                columns = {
                    {heading="Name", key="name"},
                    {heading="Description", key="description"}
                },
                sortColumn = "name",
                autospace = true,
                values = configs
            },
            accelerators = {
                grid_select = "category_select"
            }
        }
    },
    eventHandler = function(self, event) 
        if event.type == "category_select" then
            local tab = event.selected.tab
            if not self.tabs[tab.title] then
                self.tabs:add({[ tab.title ] = tab })
            end
            self.tabs:selectTab(tab)
            return true
        end
        return true
    end
}

UI:setPage(page)
UI:start()