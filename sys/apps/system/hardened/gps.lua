local Config   = require('opus.config')
local UI   = require('opus.ui')

local colors = _G.colors

local config = Config.load("hardened", {
	modems = {
		enable_spoofing = false, -- Disabled by default.
		options = {
			replyChannel = 0
		}
	},
	discovery = {
		redact_discovery = true, -- Enabled by default.
		spoof_turtle = false, -- Disabled by default.
		options = {
			label = "REDACTED",
			uptime = -913,
			status = "Health: -913",
			group = "REDACTED",
			turtle = {
				fuel = -913, -- 913 is the default.
				status = "Idle",
				point = {},
				inv = {},
				slotIndex = 0
			}
		}
	},
	gps = {
		enable_spoofing = false, -- Disabled by default.
		options = {
			x = 0,
			y = 0,
			z = 0,
			gps = false
		}
	}
})

return UI.Tab {
    title = "GPS",
    description = "GPS config",
    notification = UI.Notification { },

    test = UI.Window {
        x=2, y=2,
        ex=-2, ey=-2,
        disableHeader = true,
        backgroundColor = colors.black,
        backgroundSelectedColor = colors.black,
        unfocusedBackgroundSelectedColor = colors.black,

        enable_spoofing_checkbox = UI.Checkbox{
            x = 2, y = 2,
            label = "Enable spoofing",
            value = config.gps.enable_spoofing,
            event = "enable_spoofing_changed"
        },

        -- X, Y and Z options
        x_pos_text = UI.Text {
			value = "X Position:",
			x = 2, y = 3
		},
		x_pos_textentry = UI.TextEntry {
			x = 13, y = 3,
			width=25,height=1,
			shadowText = "X Position",
			index = 1,
            transform = "number",
        	accelerators = {enter = "set_x_pos"}
		},

        y_pos_text = UI.Text {
			value = "Y Position:",
			x = 2, y = 4
		},
		y_pos_textentry = UI.TextEntry {
			x = 13, y = 4,
			width=25,height=1,
			shadowText = "Y Position",
			index = 1,
            transform = "number",
        	accelerators = {enter = "set_y_pos"}
		},

        z_pos_text = UI.Text {
			value = "Z Position:",
			x = 2, y = 5
		},
		z_pos_textentry = UI.TextEntry {
			x = 13, y = 5,
			width=25,height=1,
			shadowText = "Z Position",
			index = 1,
            transform = "number",
        	accelerators = {enter = "set_z_pos"}
		},
    },

    eventHandler = function(self, event)
        if event.type == "enable_spoofing_changed" then
			local conf = Config.load("hardened")
			conf.gps.enable_spoofing = self.test.enable_spoofing_checkbox.value
			Config.update("hardened", conf)
            self.notification:info(("Set GPS spoofing to %s"):format(tostring(conf.gps.enable_spoofing)))
		elseif event.type == "set_x_pos" then
			local conf = Config.load("hardened")
			conf.gps.options.x = tonumber(self.test.x_pos_textentry.value)
			Config.update("hardened", conf)
            self.notification:info(("Set X Position to %d"):format(conf.gps.options.x))
		elseif event.type == "set_y_pos" then
			local conf = Config.load("hardened")
			conf.gps.options.y = tonumber(self.test.y_pos_textentry.value)
			Config.update("hardened", conf)
            self.notification:info(("Set Y Position to %d"):format(conf.gps.options.y))
		elseif event.type == "set_z_pos" then
			local conf = Config.load("hardened")
			conf.gps.options.z = tonumber(self.test.z_pos_textentry.value)
			Config.update("hardened", conf)
            self.notification:info(("Set Z Position to %d"):format(conf.gps.options.z))
        end
    end
}
