local Config   = require('opus.config')
local UI   = require('opus.ui')

local colors = _G.colors

local config = Config.load("hardened", {
	modems = {
		enable_spoofing = false, -- Disabled by default.
		options = {
			replyChannel = 0
		}
	},
	discovery = {
		redact_discovery = true, -- Enabled by default.
		spoof_turtle = false, -- Disabled by default.
		options = {
			label = "REDACTED",
			uptime = -913,
			status = "Health: -913",
			group = "REDACTED",
			turtle = {
				fuel = -913, -- 913 is the default.
				status = "Idle",
				point = {},
				inv = {},
				slotIndex = 0
			}
		}
	},
	gps = {
		enable_spoofing = false, -- Disabled by default.
		options = {
			x = 0,
			y = 0,
			z = 0,
			gps = false
		}
	}
})

return UI.Tab {
    title = "Modems",
    description = "Modem config",
    notification = UI.Notification { },

    test = UI.Window {
        x=2, y=2,
        ex=-2, ey=-2,
        disableHeader = true,
        backgroundColor = colors.black,
        backgroundSelectedColor = colors.black,
        unfocusedBackgroundSelectedColor = colors.black,

        enable_spoofing_checkbox = UI.Checkbox{
            x = 2, y = 2,
            label = "Enable spoofing",
            value = config.modems.enable_spoofing,
            event = "enable_spoofing_changed_1"
        },

        -- X, Y and Z options
        reply_channel_text = UI.Text {
			value = "Reply Channel:",
			x = 2, y = 3
		},
        reply_channel_textentry = UI.TextEntry {
			x = 16, y = 3,
			width=25,height=1,
			shadowText = "Reply channel",
			index = 1,
            transform = "number",
        	accelerators = {enter = "set_reply_channel"}
		},
    },

    eventHandler = function(self, event)
        if event.type == "enable_spoofing_changed_1" then
			local conf = Config.load("hardened")
			conf.modems.enable_spoofing = self.test.enable_spoofing_checkbox.value
			Config.update("hardened", conf)
            self.notification:info(("Set modem spoofing to %s"):format(tostring(self.test.enable_spoofing_checkbox.value)))
        elseif event.type == "set_reply_channel" then 
            local conf = Config.load("hardened")
            conf.modems.options.replyChannel = tonumber(self.test.reply_channel_textentry.value)
            Config.update("hardened", conf)
            self.notification:info(("Set modem reply channel to %d"):format(conf.modems.options.replyChannel))
        end
    end
}
