local Config   = require('opus.config')
local UI   = require('opus.ui')

local colors = _G.colors

local config = Config.load("hardened", {
	modems = {
		enable_spoofing = false, -- Disabled by default.
		options = {
			replyChannel = 0
		}
	},
	discovery = {
		redact_discovery = true, -- Enabled by default.
		spoof_turtle = false, -- Disabled by default.
		options = {
			label = "REDACTED",
			uptime = -913,
			status = "Health: -913",
			group = "REDACTED",
			turtle = {
				fuel = -913, -- 913 is the default.
				status = "Idle",
				point = {},
				inv = {},
				slotIndex = 0
			}
		}
	},
	gps = {
		enable_spoofing = false, -- Disabled by default.
		options = {
			x = 0,
			y = 0,
			z = 0,
			gps = false
		}
	}
})

return UI.Tab {
    title = "Discovery",
    description = "Discovery config",
    notification = UI.Notification { },

    test = UI.Window {
        x=2, y=2,
        ex=-2, ey=-2,
        disableHeader = true,
        backgroundColor = colors.black,
        backgroundSelectedColor = colors.black,
        unfocusedBackgroundSelectedColor = colors.black,

        redact_discovery_checkbox = UI.Checkbox{
            x = 2, y = 2,
            label = "Redact discovery",
            value = config.discovery.redact_discovery,
            event = "redact_discovery_changed"
        },
        spoof_turtle_checkbox = UI.Checkbox{
            x = 2, y = 3,
            label = "Spoof turtle",
            value = config.discovery.spoof_turtle,
            event = "spoof_turtle_changed"
        },
        -- Computer label
		computer_label_text = UI.Text {
			value = "Computer Label:",
			x = 2, y = 4
		},
		computer_label_textentry = UI.TextEntry {
			x = 17, y = 4,
			width=25,height=1,
			shadowText = "Computer Label",
			index = 1,
        	accelerators = {enter = "set_comp_label"}
		},
        -- Status
        status_text = UI.Text {
			value = "Status:",
			x = 2, y = 5
		},
		status_textentry = UI.TextEntry {
			x = 9, y = 5,
			width=25,height=1,
			shadowText = "Status",
			index = 1,
        	accelerators = {enter = "set_status"}
		},
                -- Uptime
        uptime_text = UI.Text {
            value = "Uptime:",
            x = 2, y = 6
        },
        uptime_textentry = UI.TextEntry {
            x = 9, y = 6,
            width=25,height=1,
            shadowText = "Uptime",
            index = 1,
            transform = "number",
            accelerators = {enter = "set_uptime"}
        },
        -- Group
        group_text = UI.Text {
			value = "Group:",
			x = 2, y = 7
		},
		group_textentry = UI.TextEntry {
			x = 8, y = 7,
			width=25,height=1,
			shadowText = "Network Group",
			index = 1,
        	accelerators = {enter = "set_group"}
		},

        --- Turtle options

        -- Fuel
        turtle_fuel_text = UI.Text {
			value = "Turtle Fuel:",
			x = 2, y = 9
		},
		turtle_fuel_textentry = UI.TextEntry {
			x = 14, y = 9,
			width=25,height=1,
			shadowText = "Turtle Fuel",
			index = 1,
            transform = "number",
        	accelerators = {enter = "set_turtle_fuel"}
		},

        -- Status
        turtle_status_text = UI.Text {
			value = "Turtle Status:",
			x = 2, y = 10
		},
		turtle_status_textentry = UI.TextEntry {
			x = 16, y = 10,
			width=25,height=1,
			shadowText = "Turtle Status",
			index = 1,
        	accelerators = {enter = "set_turtle_status"}
		},

        -- Slot Index
        turtle_slot_index_text = UI.Text {
			value = "Turtle Slot Index:",
			x = 2, y = 11
		},
		turtle_slot_index_textentry = UI.TextEntry {
			x = 20, y = 11,
			width=25,height=1,
			shadowText = "Turtle Slot Index",
			index = 1,
        	accelerators = {enter = "set_turtle_slot_index"}
		},
    },

    eventHandler = function(self, event)
        if event.type == "redact_discovery_changed" then
			local conf = Config.load("hardened")
			conf.discovery.redact_discovery = self.test.redact_discovery_checkbox.value
			Config.update("hardened", conf)
            self.notification:info(("Set redact discovery to %s"):format(tostring(conf.discovery.redact_discovery)))
        elseif event.type == "spoof_turtle_changed" then
            local conf = Config.load("hardened")
            conf.discovery.spoof_turtle = self.test.spoof_turtle_checkbox.value
            Config.update("hardened", conf)
            self.notification:info(("Set turtle spoofing to %s"):format(tostring(conf.discovery.spoof_turtle)))
		elseif event.type == "set_comp_label" then
			local conf = Config.load("hardened")
			conf.discovery.options.label = self.test.computer_label_textentry.value
			Config.update("hardened", conf)
            self.notification:info(("Set computer label to %s"):format(tostring(conf.discovery.options.label)))
		elseif event.type == "set_status" then
			-- status_textentry
			local conf = Config.load("hardened")
			conf.discovery.options.status = self.test.status_textentry.value
			Config.update("hardened", conf)
            self.notification:info(("Set status to %s"):format(tostring(conf.discovery.options.status)))
		elseif event.type == "set_uptime" then
			-- uptime_textentry
			local conf = Config.load("hardened")
			conf.discovery.options.uptime = tonumber(self.test.uptime_textentry.value)
			Config.update("hardened", conf)
            self.notification:info(("Set uptime to %d"):format(conf.discovery.options.uptime))
		elseif event.type == "set_group" then
			-- group_textentry
			local conf = Config.load("hardened")
			conf.discovery.options.group = self.test.group_textentry.value
			Config.update("hardened", conf)
            self.notification:info(("Set group to %s"):format(tostring(conf.discovery.options.group)))
		elseif event.type == "set_turtle_fuel" then
			local conf = Config.load("hardened")
			conf.discovery.options.turtle.fuel = self.test.turtle_fuel_textentry.value
			Config.update("hardened", conf)
            self.notification:info(("Set turtle fuel to %s"):format(tostring(conf.discovery.options.turtle.fuel)))
		elseif event.type == "set_turtle_status" then
			local conf = Config.load("hardened")
			conf.discovery.options.turtle.status = self.test.turtle_status_textentry.value
			Config.update("hardened", conf)
            self.notification:info(("Set turtle status to %s"):format(tostring(conf.discovery.options.turtle.status)))
		elseif event.type == "set_turtle_slot_index" then
			local conf = Config.load("hardened")
			conf.discovery.options.turtle.slotIndex = self.test.turtle_slot_index_textentry.value
			Config.update("hardened", conf)
            self.notification:info(("Set turtle slot index to %s"):format(tostring(conf.discovery.options.turtle.slotIndex)))
        end
    end
}
