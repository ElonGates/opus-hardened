## Disclaimer!
This might get you banned from a server. **We take no responsibilities for damage done.**
###### Always make sure to check the server's rules before using opus-hardened.


# Hardened Opus OS for ComputerCraft
This is a fork of Opus OS by kepler15cc, but we have removed discovery information such as uptime and the label.

<img src="https://github.com/kepler155c/opus-wiki/blob/master/assets/images/opus.gif?raw=true" width="540" height="360">

## Features
* Sending fake data to discovery
* Multitasking OS - run programs in separate tabs
* Telnet (wireless remote shell)
* VNC (wireless screen sharing)
* UI API
* Turtle API (includes true pathfinding based on the ASTAR algorithm)
* Remote file system access (you can access the file system of any computer in wireless range)
* File manager
* Lua REPL with GUI
* Run scripts on single or groups of computers (GUI)
* Turtle follow (with GPS) and turtle come to you (without GPS)

## Install
```
wget run https://codeberg.org/ElonGates/opus-hardened/raw/branch/main/pcinstall.lua
```
